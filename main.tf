resource "docker_container" "sonarqube" {
	name ="sonarqube" 
	image ="sonarqube:7.9-community"
	ports {
    	internal = "9200"
		external = "9200"
  	}
}